from threading import Thread
from time import sleep
from binbinTimer import *
import sys

bleno = Bleno()

# BLE inti
timer = BinbinTimer(bleno)

# BLE Start Characteristic
characteristic = BinbinCharacteristic('', timer)

# BLE Start Service
BinbinService(bleno, characteristic, timer)

