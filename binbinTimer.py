from threading import Thread, Timer
from time import sleep

class BinbinTimer():

  def __init__(self, bleno, timeout):
    self.bleno = bleno
    self.timeout = timeout
    self.flag = False
    

  def startTime(self):
    print('Start Time')
    self.flag = False
    self.time = Timer(self.timeout, self.endTime)
    self.time.start()

  def restartTime(self):
    print('Restart Time')
    self.time.cancel()
    self.startTime()

  def endTime(self):
    print('End Time')

    self.bleno.disconnect()
    self.flag = True
    self.time.cancel()
