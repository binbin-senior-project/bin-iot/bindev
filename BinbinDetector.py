import os
import sys
from timeit import default_timer as timer

def get_parent_dir(n=1):

	current_path = os.path.dirname(os.path.abspath(__file__))
	for k in range(n):
		current_path = os.path.dirname(current_path)
	print(current_path)
	return current_path

src_path = os.path.join(get_parent_dir(0))
yolo3_path = os.path.join(get_parent_dir(0),'TrainYourOwnYOLO','2_Training','src')
util_path = os.path.join(get_parent_dir(0),'TrainYourOwnYOLO','Utils')

print(yolo3_path)

sys.path.append(src_path)
sys.path.append(yolo3_path)
sys.path.append(util_path)

from keras_yolo3.yolo import YOLO
from utils import detect_object

def loadModel():
	yolo = YOLO(
					**{
							"model_path": './trained_weights_final2.h5',
							"anchors_path": './TrainYourOwnYOLO/2_Training/src/keras_yolo3/model_data/yolo-tiny_anchors.txt',
							"classes_path": './data_classes.txt',
							"score": 0.25,
							"gpu_num": 1,
							"model_image_size": (416, 416),
					}
			)
	return yolo


def detectTrash(img_path,yolo):
	print('detectTrash')
	start = timer()
	# img_path = 'test.jpg'
	prediction, image = detect_object(
									yolo,
									img_path,
									save_img=True,
									save_img_path="",
									postfix="_bank"
							)
	end = timer()
	print('time spend', end - start)
	return prediction