from pybleno import Characteristic
import array
import struct
import sys
import traceback
from BinbinDetector import *
from binbinTimer import *

class BinbinCharacteristic(Characteristic):
    yolo = None
    qrcode = '1234'
    correctQRFlag = False;

    def __init__(self, uuid, timmer):
        Characteristic.__init__(self, {
            'uuid': uuid,
            'properties': ['read', 'write', 'notify'],
            'value': None
          })
          
        self._value = array.array('B', [0] * 0)
        self._updateValueCallback = None
        self.yolo = loadModel()
        self.timmer = timmer
          
    def onReadRequest(self, offset, callback):
        print('EchoCharacteristic - %s - onReadRequest: value = %s' % (self['uuid'], [hex(c) for c in self._value]))
        callback(Characteristic.RESULT_SUCCESS, self._value[offset:])

    def onWriteRequest(self, data, offset, withoutResponse, callback):
        # self.timmer.restartTime()
        # print("Input value", data.decode("utf-8"))
        # inputText = data.decode("utf-8")
        # print('EchoCharacteristic - %s - onWriteRequest: value = %s' % (self['uuid'], [hex(c) for c in self._value]))

        # if self._updateValueCallback:

        #     if inputText == self.qrcode:

        #         print("passQR")
        #         self.correctQRFlag = True
        #         self._value = b"passQR"
        #         self._updateValueCallback(self._value)    
        #         callback(Characteristic.RESULT_SUCCESS)

        #         return
                

        #     if inputText == 'Fin':

        #         print("Fin")
        #         self.correctQRFlag = False
        #         self._value = b"fin"
        #         self._updateValueCallback(self._value)
        #         callback(Characteristic.RESULT_SUCCESS)
        #         return

        #     if(self.correctQRFlag and inputText == "getTrash"):

        #         print("trashType")
        #         img_path = 'test.jpg'
        #         # predict = detectTrash(img_path,self.yolo)
        #         print(predict)
        #         if predict[0][4] == 0:
        #             self._value = b"can"
        #         elif predict[0][4] == 1:
        #             self._value = b"glass"
        #         elif predict[0][4] == 2:
        #             self._value = b"plastic"
        #         else:
        #             self._value = b"other"

        #     else:

        #         self._value = b"failQR"

        # else:
        #     print("NotSub")
        #     self._value = b"NotSub"
            
        # print("ora")
        self._updateValueCallback(self._value)
        callback(Characteristic.RESULT_SUCCESS)
        
    def onSubscribe(self, maxValueSize, updateValueCallback):
        print('EchoCharacteristic - onSubscribe')
        
        self._updateValueCallback = updateValueCallback
        for i in range(100):

            if self.timmer.flag:
                break
                
            img_path = 'test.jpg'
            predict = detectTrash(img_path,self.yolo)
            print(predict)
            if predict[0][4] == 0:
                self._value = b"can"
            elif predict[0][4] == 1:
                self._value = b"glass"
            elif predict[0][4] == 2:
                self._value = b"plastic"
            else:
                self._value = b"other"

            self._updateValueCallback(self._value)
            # sleep(10)

    def onUnsubscribe(self):
        print('EchoCharacteristic - onUnsubscribe');
        
        self._updateValueCallback = None
