from pybleno import *
import sys
import signal
from BinbinCharacteristic import *
from binbinTimer import *



print('bleno - echo');

bleno = Bleno()
timmer = BinbinTimer(bleno,30)

def onStateChange(state):
   print('on -> stateChange: ' + state);

   if (state == 'poweredOn'):
     bleno.startAdvertising('echo', ['1170362D42760598B2D7F0B8FF899FE1'])
   else:
     bleno.stopAdvertising();

def onAdvertisingStart(error):
    print('on -> advertisingStart: ' + ('error ' + error if error else 'success'));

    if not error:
        bleno.setServices([
            BlenoPrimaryService({
                'uuid': '1170362D42760598B2D7F0B8FF899FE1',
                'characteristics': [ 
                    BinbinCharacteristic('ec0F',timmer)
                    ]
            })
        ])

def onDisconneted(clientAddress):
    timmer.flag = True
    print("onDisconneted",clientAddress)

def onAccept(clientAddress):
    timmer.startTime()
    print("Accepted",clientAddress)

bleno.on('advertisingStart', onAdvertisingStart)
bleno.on('accept',onAccept)
bleno.on('disconnect',onDisconneted)

bleno.on('stateChange', onStateChange)        
bleno.on('advertisingStart', onAdvertisingStart)

bleno.start()

print ('Hit <ENTER> to disconnect')

if (sys.version_info > (3, 0)):
    input()
else:
    raw_input()

bleno.stopAdvertising()
bleno.disconnect()

print ('terminated.')
sys.exit(1)
